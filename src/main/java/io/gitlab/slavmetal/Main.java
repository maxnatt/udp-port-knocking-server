/*******************************************************************************
 * Copyright (c) 2018 SlavMetal.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the accompanying LICENSE file.
 *******************************************************************************/
package io.gitlab.slavmetal;

import java.util.Arrays;

public class Main {
    /**
     * Entry point.
     * @param args space-delimited UDP ports to open.
     */
    public static void main(String[] args) {
        new UDPServer(Arrays.asList(args)).startServer();
    }
}
